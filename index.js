import AjaxForm from './src/form/AjaxForm';
import Form from './src/form/Form';
import FormButton from './src/buttons/FormButton';
import validationRules from './src/validation/rules';

module.exports = {
    AjaxForm,
    Form,
    FormButton,
    validationRules
}
