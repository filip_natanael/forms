import { getEl, getEls, forEach, extend } from '@superskrypt/sutils';

const defaults = {};

export default class Button {

	/**
	 * Initialises the class
	 * @param  {string or DOMElement} $el   a selector or passed DOMElement of the
	 *                                      <input type="button"> we want to handle
	 * @return this
	 */
	constructor( el, options ) {

		const button = getEl( el );

		if ( button ) {

			this.init( button, options );

		} else {

			console.log( 'BUTTON ELEMENT DOESNT EXIST' );

		}

	}

	init( buttonEl, options ) {

		this.el = buttonEl;
		this.options = extend( defaults, options );

		this.initEvents();

	}

	initEvents() {

		this.el.addEventListener( 'click', this.clickHandler.bind( this ) );

	}

	// we add the Handler so the class
	// serves as something resembling an Interface Class
	clickHandler() {}

}
