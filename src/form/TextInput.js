import Input from './Input';


export default class TextInput extends Input {

	constructor( inputEl ) {

		super( inputEl );

		this.filled = false;

	}

	clear() {

		this.el.value = '';

	}

}
