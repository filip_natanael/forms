import TextInput from './TextInput';
import TextareaInput from './TextareaInput';
import CheckboxInput from './CheckboxInput';
import RadioInput from './RadioInput';
import NumberInput from './NumberInput';
import FileInput from './FileInput';

const inputs = {};

class InputFactory {

	static register( inputType, inputClass ) {

		inputs[inputType] = inputClass;

	}

	static create( inputEl ) {

		const Creatable = inputs[inputEl.type];

		// it creates a new Proxy
		// so 

		return new Proxy( new Creatable( inputEl ), {
			get: ( target, name, receiver ) => {

				return target.el && name in target.el
					? target.el[name]
					: target[name];

			}
		} );

	}

}

InputFactory.register( 'text', TextInput );
InputFactory.register( 'email', TextInput );
InputFactory.register( 'hidden', TextInput );
InputFactory.register( 'password', TextInput );
InputFactory.register( 'textarea', TextareaInput );
InputFactory.register( 'checkbox', CheckboxInput );
InputFactory.register( 'radio', CheckboxInput );
InputFactory.register( 'number', NumberInput );
InputFactory.register( 'file', FileInput );

export default InputFactory;