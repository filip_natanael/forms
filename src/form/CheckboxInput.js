import Input from './Input';

export default class CheckboxInput extends Input {

	constructor( inputEl ) {

		super( inputEl );

		// this.initCheckboxEvents();

	}

	clear() {

		this.el.checked = false;

	}

	initCheckboxEvents() {

		this.el.addEventListener( 'click', this.clickHandler.bind( this ) );

	}


	clickHandler( e ) {

		this.trigger( 'checkbox:click', { input: this, event: e } );

		this.trigger( 'filled', { input: this, filled: this.el.checked } );

	}

	// focus and blur are overwritten so those event don't propagate up
	// and don't trigger generic Input UI responses
	focusHandler() {}
	blurHandler() {}

}