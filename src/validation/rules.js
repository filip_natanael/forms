import { getEl, getEls } from '@superskrypt/sutils';

const labelName = input => {

	if ( input.labels.length > 0 ) {

		if( input.labels[0])

		return input.labels[0].innerText;

	}

	return input.id;

};


const getFile = input => {

	// if there is custom file upload logic, for example, images are resized client-side
    // generated Blobs should be assigned to fileInput._file
    // and can be sent via ajax with FormData

    // if file was deleted, custom field can be set to an empty string

    // Bunny Validation detects if there is custom Blob assigned to file input
    // and uses this file for validation instead of original read-only input.files[]
    if (input._file !== undefined && input._file !== '') {
        if (input._file instanceof Blob === false) {
            console.error(`Custom file for input ${input.name} is not an instance of Blob`);
            return false;
        }
        return input._file;
    }

    return input.files[0] || false;

};

const rules =  {
	required: {
		validator: input => {

			if ( input.getAttribute( 'type' ) !== 'file' && input.value === ''
                || ( ( input.type === 'radio' || input.type === 'checkbox' ) && !input.checked )
                || input.getAttribute( 'type' ) === 'file' && getFile( fileInput ) === false ) {

				return false;
			}

			return true;

		},
		message: input => {

			if ( input.getAttribute( 'type' ) === 'checkbox' ) {

				return 'Zaznaczenie pola jest wymagane';

			} else {

				return `Uzupełnienie pola ${labelName( input )} jest wymagane`

			}

		}
	},
	minlength: {
		validator: input => input.value.length >= input.getAttribute( 'minlength' ),
		message: input => `Pole ${labelName( input )} musi posiadać przynajmniej ${input.getAttribute( 'minlength' )} znaki`
	},
	maxlength: {
		validator: input => input.value.length < input.getAttribute( 'maxlength' ),
		message: input => `Pole ${labelName( input )} może zawierać maksymalnie ${input.getAttribute( 'maxlength' )} znaków`
	},
	telephone: {
		validator: input => {

			const phoneRegex = /^[0-9\+]{9,15}$/;
			return phoneRegex.test( input.value );

		},
		message: 'Numer powinien zawierać co najmniej 9 znaków.'
	},
	password: {
		validator: ( value ) => {

			const passwordsRegex = /(?=.{5,})/;

			return passwordsRegex.test( value );

		},
		message: 'Minimalna długość hasła to 5 znaków.'
	},
	name: {
		validator: input => {

			const { value } = input;

			let flag = false;
			const splited = value.split( ' ' );

			if ( splited.length === 2 && splited[0].length > 1 && splited[1].length > 1 ) {

				flag = true;

			}

			return flag;

		},
		message: 'Imię i nazwisko muszą zawierać co najmniej po 2 znaki.'
	},
	email: {
		validator: input => {

			if ( input.value.length > 0 && input.getAttribute( 'type' ) === 'email' ) {

				const emailRegex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

				return emailRegex.test( input.value );

			}

			return true;

		},
		message: input => `Pole ${labelName( input )} nie jest poprawnym adresem email`
	},
	optionalequalorsmallerthan: {
		validator: input => {

			const compareTo = ( input.getAttribute('optionalequalorsmallerthan') );
			const compareToEl = getEl( `#${compareTo}` );

			if ( compareToEl.value && parseInt( input.value, 10 ) > parseInt( compareToEl.value, 10 ) ) {

				return false;

			}

			return true;


		},
		message: input => {

			const compareTo = ( input.getAttribute('optionalequalorsmallerthan') );
			const compareToEl = getEl( `#${compareTo}` );

			return `Pole ${labelName( input )} powinno być większe niż pole  ${labelName( compareToEl )}`
		}
	},
	optionalequalorbiggerthan: {
		validator: input => {

			const compareTo = ( input.getAttribute('optionalequalorbiggerthan') );
			const compareToEl = getEl( `#${compareTo}` );

			if ( compareToEl.value && parseInt( input.value, 10 ) < parseInt( compareToEl.value, 10 ) ) {

				return false;

			}

			return true;


		},
		message: input => {

			const compareTo = ( input.getAttribute('optionalequalorbiggerthan') );
			const compareToEl = getEl( `#${compareTo}` );

			return `Pole ${labelName( input )} powinno być mniejsze niż pole  ${labelName( compareToEl )}`
		}
	},

	// input Group rules
	minCheckedCount: {
		validator: inputGroup => {

			const minCount = inputGroup.getAttribute( 'ig-min-checked-count' );
			const checked = getEls( ':checked', inputGroup );

			return checked.length >= minCount;

		},
		message: inputGroup => {

			const minCount = inputGroup.getAttribute( 'ig-min-checked-count' );
			const labelName = inputGroup.classRef.inputs[0].name;
			const label = getEl( `label[for="${labelName}"]` ).innerText;

			return `Proszę zaznaczyć co najmniej ${minCount} pole dla ${label}`;

		}
	},

	inputRequiredIfChecked: {
		validator: inputGroup => {

			const checkedId = inputGroup.getAttribute( 'input-required-if-checked' );
			const inputCheckedId = getEl(' input[type=text] ', inputGroup);
			const checked = getEls(' :checked ', inputGroup);

			if( checked.length >= 1 ) {

				if( checked[0].getAttribute('id') == checkedId ) {

					return inputCheckedId.value.trim();

				}

			} else {

				return false;

			}


			return true;

		},
		message: inputGroup => {

			const checkedId = inputGroup.getAttribute( 'input-required-if-checked' );
			const inputCheckedId = getEl(' input[type=text] ', inputGroup);
			const checked = getEls(' :checked ', inputGroup);
			const labelName = getEls(' input[type=radio] ', inputGroup)[0].name;
			const label = getEl( `label[for="${labelName}"]` ).innerText;

			if( checked.length >= 1 ) {

				if( checked[0].getAttribute('id') == checkedId && !inputCheckedId.value.trim() ) {

					return `Proszę uzupełnić pole dla zaznaczonej opcji inny`;

				}

			} else {

				return `Proszę zaznaczyć co najmniej 1 pole dla ${label}`;

			}

		}
	}

};

export default rules;
